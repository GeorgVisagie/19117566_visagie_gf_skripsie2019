#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

#define TEMP_AND_FLOW  0
#define CT_AND_RELAY   1
#define UI             2
#define CONTROL_CHANGE 3

#define VALVE_IDLE    0
#define VALVE_OPENING 1
#define VALVE_CLOSING 2

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
#define OLED_RESET     -1 // Reset pin # (or -1 if sharing Arduino reset pin)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

unsigned long currentDisplayTime = 0;
unsigned long lastDisplayTime = 0;    // for display screen update
const long displayDelay = 500; // update every (x) ms

float sampleCurrent;
float rmsCurrentSum;
const int adc_curr_max = 1023; //1023
const int adc_curr_min = 0; //-19 met voltage divider
const int currentTransformerPin = A2;
int adcCurrent = 0;
int currentSampleCounter = 0;
float currentRMS = 0.0;
float Wh = 0.0;
float apparentPower = 0.0;
const long oneHour = 3600000; // 3 600 000 milliseconds in an hour

const int currentN = 1000; // Number of samples
const long currentDelay = 1; //sample every (x) ms
unsigned long lastCurrentTime = 0;
unsigned long currentCurrentTime = 0;

float tempIN = 0.0;   // stores the calculated temperature
int tempSampleCounter = 0;                // counts through ADC samples
float samplesIN = 0.0;   // stores sum of 10 samples
float tempOUT = 0.0;   // stores the calculated temperature
float samplesOUT = 0.0;   // stores sum of 10 samples

float tempINDisp;
float tempOUTDisp;
const float tempN = 100.0; // Number of samples
const long tempDelay = 10; //sample every (x) ms
unsigned long lastTempTime = 0;
unsigned long currentTempTime = 0;

#define flowInterrupt 4  // 1 = digital pin 2
#define flowPin 7

const int tempPinIN = A0;
const int tempPinOUT = A1;
const int tempCalibrationFactor = 1038;

const int relayPin = 10;
bool relayOn = false;
unsigned long currentRelayTime = 0;
unsigned long lastRelayTime = 0;
const long relayDelay = 350; //350 verander

long relayOnStartTime = 0;
long relayOnCurrentTime = 0;
long relayOnElapsedTime = 0;
long lastElapsedTime = 0;

// The hall-effect flow sensor outputs approximately 8.1 pulses per second per
// litre/minute of flow.
float calibrationFactor = 4.5;
volatile byte pulseCount = 0;
float flowRate = 0.0;
float flowMilliLitres = 0.0;
float totalMilliLitres = 0.0;
unsigned long lastFlowTime = 0;


#define pinA 11 // Connected to CLK / Pin A
#define pinB 12 // Connected to DT / Pin B
#define button1Pin 6
#define button2Pin 4
#define valveOpenPin 8 // BLUE
#define valveClosePin 9 // RED

unsigned long currentValveTime = 0;
unsigned long lastValveTime = 0;
const long valveDelay = 350; // open or close valve for (x) ms gives 10 steps for the valve

unsigned long currentTempControlTime = 0;
unsigned long lastTempControlTime = 0;
const long tempControlDelay = 5000; // delay before attempting to increase/decrease water temperature by adjusting flow

unsigned long currentControlChangeTime = 0;
unsigned long lastControlChangeTime = 0;
const long controlChangeDelay = 1000; // show that control method changed

unsigned long lastExecutionTime = 0;
long executionTime = 0;

bool button1State = HIGH;
bool lastButton1State = HIGH;
bool button2State = HIGH;
bool lastButton2State = HIGH;
bool changeTemp = true;
unsigned long lastButton1Time = 0;  // the last time the output pin was toggled
unsigned long button1Delay = 50;    // the debounce time; increase if the output flickers
unsigned long lastButton2Time = 0;  // the last time the output pin was toggled
unsigned long button2Delay = 50;    // the debounce time; increase if the output flickers

int tempSetpoint = 15;

// Counter that will be incremented or decremented by rotation.
int displayState = 1;
int lastDisplayState = 1;
int valveState = 0;

static uint8_t prevNextCode = 0;
static uint16_t storeCode = 0;
static int8_t rotEncTable[] = {0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0};
static int8_t encoderVal;

static const unsigned char PROGMEM taptula_bmp[] =
{ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x0F, 0xCE, 0x1F, 0x7E, 0xCC, 0xC1, 0xC0, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x03, 0x0E, 0x19, 0x98, 0xCC, 0xC1, 0xC0, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x03, 0x1B, 0x19, 0x98, 0xCC, 0xC3, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x03, 0x1B, 0x19, 0x98, 0xCC, 0xC3, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x03, 0x1B, 0x1F, 0x18, 0xCC, 0xC3, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x03, 0x3F, 0x98, 0x18, 0xCC, 0xC7, 0xF0, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x03, 0x31, 0x98, 0x18, 0xCC, 0xC6, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x03, 0x31, 0x98, 0x18, 0x78, 0xFE, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x03, 0xE0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xF8, 0x00, 0x00,
  0x00, 0x0F, 0xFC, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x07, 0xFE, 0x00, 0x00,
  0x00, 0x1F, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1F, 0xFF, 0x00, 0x00,
  0x00, 0x3F, 0xFF, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3F, 0xFF, 0x80, 0x00,
  0x00, 0x7F, 0xFF, 0xE0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xC0, 0x00,
  0x00, 0x7F, 0xFF, 0xF0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0xFF, 0xFF, 0xC0, 0x00,
  0x00, 0x7F, 0x1F, 0xF8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x07, 0xFF, 0x1F, 0xC0, 0x00,
  0x00, 0xFE, 0x07, 0xFE, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0F, 0xFC, 0x0F, 0xE0, 0x00,
  0x00, 0xFC, 0x03, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1F, 0xF8, 0x07, 0xE0, 0x00,
  0x00, 0xFC, 0x00, 0xFF, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x3F, 0xE0, 0x07, 0xE0, 0x00,
  0x01, 0xF8, 0x00, 0x7F, 0xE0, 0x00, 0x1F, 0xFF, 0xFC, 0x00, 0x00, 0xFF, 0xC0, 0x03, 0xF0, 0x00,
  0x01, 0xF8, 0x00, 0x3F, 0xF0, 0x00, 0xFF, 0xFF, 0xFF, 0xC0, 0x01, 0xFF, 0x80, 0x03, 0xF0, 0x00,
  0x01, 0xFF, 0x80, 0x1F, 0xFC, 0x07, 0xFF, 0xFF, 0xFF, 0xF8, 0x07, 0xFF, 0x00, 0x1F, 0xF0, 0x00,
  0x01, 0xFF, 0xF0, 0x07, 0xFE, 0x1F, 0xFF, 0xFF, 0xFF, 0xFE, 0x0F, 0xFC, 0x01, 0xFF, 0xF0, 0x00,
  0x01, 0xFF, 0xFE, 0x03, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x3F, 0xF8, 0x0F, 0xFF, 0xF0, 0x00,
  0x03, 0xFF, 0xFF, 0xC1, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xF0, 0x7F, 0xFF, 0xF8, 0x00,
  0x03, 0xFF, 0xFF, 0xF0, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xE1, 0xFF, 0xFF, 0xF8, 0x00,
  0x03, 0xFF, 0xFF, 0xFC, 0x3F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x87, 0xFF, 0xFF, 0xF8, 0x00,
  0x03, 0xFC, 0x7F, 0xFF, 0x1F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x1F, 0xFF, 0xC7, 0xF8, 0x00,
  0x03, 0xF8, 0x0F, 0xFF, 0xDF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFE, 0x7F, 0xFE, 0x03, 0xF8, 0x00,
  0x03, 0xF8, 0x01, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xF0, 0x01, 0xF8, 0x00,
  0x03, 0xF0, 0x00, 0x7F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xC0, 0x01, 0xF8, 0x00,
  0x03, 0xF0, 0x00, 0x3F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x80, 0x01, 0xF8, 0x00,
  0x03, 0xF0, 0x00, 0x0F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFE, 0x00, 0x01, 0xF8, 0x00,
  0x03, 0xF0, 0x7F, 0xE3, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xF8, 0xFF, 0xC1, 0xF8, 0x00,
  0x03, 0xF1, 0xFF, 0xF8, 0xFF, 0xFF, 0x9F, 0xFF, 0xFC, 0xFF, 0xFF, 0xC3, 0xFF, 0xF1, 0xF8, 0x00,
  0x03, 0xF1, 0xFF, 0xFE, 0xFF, 0xFE, 0x07, 0xFF, 0xF0, 0x3F, 0xFF, 0xCF, 0xFF, 0xF1, 0xF8, 0x00,
  0x03, 0xF3, 0xFF, 0xFF, 0xFF, 0xFC, 0x63, 0xFF, 0xF3, 0x9F, 0xFF, 0xFF, 0xFF, 0xF9, 0xF8, 0x00,
  0x03, 0xF3, 0xFF, 0xFF, 0xFF, 0xFC, 0xF9, 0xFF, 0xEF, 0xCF, 0xFF, 0xFF, 0xFF, 0xF9, 0xF8, 0x00,
  0x03, 0xF7, 0xF1, 0xFF, 0xFF, 0xF9, 0xFD, 0xFF, 0xCF, 0xCF, 0xFF, 0xFF, 0xF1, 0xFD, 0xF8, 0x00,
  0x03, 0xF7, 0xE0, 0x3F, 0xFF, 0xFB, 0xFD, 0xFF, 0xCF, 0xEF, 0xFF, 0xFF, 0x80, 0xFD, 0xF8, 0x00,
  0x03, 0xF7, 0xE0, 0x0F, 0xFF, 0xFF, 0xFD, 0xFF, 0xDF, 0xFF, 0xFF, 0xFE, 0x00, 0xFD, 0xF8, 0x00,
  0x01, 0xF7, 0xE0, 0x03, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xF8, 0x00, 0xFD, 0xF0, 0x00,
  0x01, 0xF7, 0xC0, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xE0, 0x00, 0x7D, 0xF0, 0x00,
  0x01, 0xF7, 0xC0, 0x00, 0x7F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x80, 0x00, 0x7D, 0xF0, 0x00,
  0x01, 0xFF, 0xC0, 0x00, 0x7F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x7F, 0xF0, 0x00,
  0x00, 0xEF, 0xC0, 0x00, 0x3F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x7E, 0xE0, 0x00,
  0x00, 0x0F, 0xC0, 0x00, 0x1F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFE, 0x00, 0x00, 0x7E, 0x00, 0x00,
  0x00, 0x07, 0xC0, 0x00, 0x0F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFC, 0x00, 0x00, 0x7E, 0x00, 0x00,
  0x00, 0x07, 0xC0, 0x00, 0x07, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xF8, 0x00, 0x00, 0x7C, 0x00, 0x00,
  0x00, 0x07, 0xC0, 0x00, 0x03, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xF0, 0x00, 0x00, 0x7C, 0x00, 0x00,
  0x00, 0x07, 0xC0, 0x00, 0x01, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xC0, 0x00, 0x00, 0x7C, 0x00, 0x00,
  0x00, 0x07, 0xE0, 0x00, 0x00, 0x7F, 0xFF, 0xFF, 0xFF, 0xFF, 0x80, 0x00, 0x00, 0xFC, 0x00, 0x00,
  0x00, 0x07, 0xE0, 0x00, 0x00, 0x1F, 0xFF, 0xFF, 0xFF, 0xFE, 0x00, 0x00, 0x00, 0xFC, 0x00, 0x00,
  0x00, 0x07, 0xE0, 0x00, 0x00, 0x07, 0xFF, 0xFF, 0xFF, 0xF8, 0x00, 0x00, 0x00, 0xFC, 0x00, 0x00,
  0x00, 0x07, 0xE0, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0xC0, 0x00, 0x00, 0x00, 0xFC, 0x00, 0x00,
  0x00, 0x07, 0xE0, 0x00, 0x00, 0x00, 0x0F, 0xFF, 0xFC, 0x00, 0x00, 0x00, 0x00, 0xFC, 0x00, 0x00,
  0x00, 0x03, 0xC0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x78, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};

void setup() {
  Serial.begin(57600);

  pinMode(flowPin, INPUT);
  digitalWrite(flowPin, HIGH);

  pinMode(valveOpenPin, OUTPUT);    // sets the digital pin 12 as output OPEN Valve   RED Wire
  pinMode(valveClosePin, OUTPUT);    // sets the digital pin 13 as output CLOSE Valve  BLUE Wire
  pinMode(relayPin, OUTPUT);
  pinMode(button1Pin, INPUT);
  pinMode(button2Pin, INPUT);
  pinMode(pinA, INPUT);
  pinMode(pinB, INPUT);

  attachInterrupt(flowInterrupt, pulseCounter, FALLING);

  initDisplay();
}

void loop() {
  //lastExecutionTime = micros();

  sampleTemperature();
  readButtons();
  doRotaryEncoder();
  checkValve();
  checkRelay();
  findRMSCurrent();
  calculateFlow();
  updateDisplay();

  //executionTime = micros() - lastExecutionTime;
  // Serial.println(executionTime);

}

void initDisplay() {
  if (!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3C for 128x64
    Serial.println(F("SSD1306 allocation failed"));
    for (;;); // Don't proceed, loop forever
  }
  display.display();
  delay(1000); // Pause for 2 seconds

  // Clear the buffer.
  display.clearDisplay();

  display.drawBitmap(0, 0, taptula_bmp, 128, 64, 1);
  display.display();
  delay(1000);
  display.clearDisplay();

  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(10, 30);
  display.clearDisplay();
  display.println("Smart Tap"); // Startup screen
  display.display();
  delay(1000);
  display.clearDisplay();
}

void sampleTemperature() {
  tempIN = 0.0;
  tempOUT = 0.0;
  // take samples from the MCP9700 temperature sensor
  currentTempTime = millis();
  if (currentTempTime - lastTempTime >= tempDelay)
  {
    tempIN = (((float)analogRead(tempPinIN) * 5.0 / 1023.0) - 0.5) * tempCalibrationFactor / 1000;
    tempIN = tempIN / 0.01;

    tempOUT = (((float)analogRead(tempPinOUT) * 5.0 / 1023.0) - 0.5) * tempCalibrationFactor / 1000;
    tempOUT = tempOUT / 0.01;


    samplesIN = samplesIN + tempIN;   //sum all the samples
    samplesOUT = samplesOUT + tempOUT;
    tempSampleCounter++;

    lastTempTime = currentTempTime;
  }

  // get the average value of 10 temperatures
  if (tempSampleCounter == tempN) {

    tempINDisp = samplesIN / tempN;
    samplesIN = 0.0;

    tempOUTDisp = samplesOUT / tempN;
    samplesOUT = 0.0;

    tempSampleCounter = 0;
  }
}

void updateDisplay() {
  currentDisplayTime = millis();
  if (currentDisplayTime - lastDisplayTime >= displayDelay)
  {

    display.setTextSize(1.5);
    display.setTextColor(WHITE);
    display.setCursor(0, 0);
    display.clearDisplay();

    switch (displayState)
    {
      case TEMP_AND_FLOW:
        displayTempAndFlow();
        break;
      case CT_AND_RELAY:
        displayCTAndRelay();
        break;
      case UI:
        displayUI();
        break;
      case CONTROL_CHANGE:
        displayControlChange();
        break;
      default:
        break;
    }

    display.display();
    lastDisplayTime = currentDisplayTime; // save the last time you changed the display
  }
}

void displayTempAndFlow() {

  if (changeTemp) {
    display.print(">");
  }
  display.print("Temp Setpoint: ");
  display.print(tempSetpoint); display.print("");
  display.print((char)247); display.println("C");
  display.print("Temp IN: ");
  display.print(tempINDisp); display.print("");
  display.print((char)247); display.println("C");
  display.print("Temp OUT: ");
  display.print(tempOUTDisp); display.print("");
  display.print((char)247); display.println("C");

  if (!changeTemp) {
    display.print(">");
  }
  display.println("Flow rate: ");
  display.print(flowRate);
  display.println(" L/min");
  display.println("Vol Flowed: ");
  display.print(totalMilliLitres);
  display.println(" mL  ");
  display.print(totalMilliLitres / 1000);
  display.println("L");
}

void displayCTAndRelay() {
  display.print("Heating Element: ");
  if (relayOn) {
    display.println("ON");
  } else {
    display.println("OFF");
  }
  display.print("Current RMS: ");
  display.print(currentRMS);
  display.println(" A");
  display.println("Apparent Power: ");
  display.print(currentRMS * 230.00);
  display.println(" Watt");
  display.println("Power Consumption ");
  display.print(Wh);
  display.println(" Wh");
  //display.print("Elapsed Time: ");
  //display.print(relayOnElapsedTime);
}

void displayUI() {
  display.setTextSize(1.5);
  display.println("WATER OUT:");
  display.setTextSize(2.5);
  display.print((int)tempOUTDisp); display.print("");
  display.print((char)247); display.println("C");
  display.setTextSize(2.5);
  display.print((int)flowRate);
  display.println(" L/min");
  display.print((int)Wh);
  display.println(" Wh");
}

void displayControlChange() {
  currentControlChangeTime = millis();
  if (currentControlChangeTime - lastControlChangeTime >= controlChangeDelay) {
    displayState = lastDisplayState;
    lastControlChangeTime = currentControlChangeTime;
  }

  display.setTextSize(2.5);
  if (changeTemp) {
    display.println("");
    display.println("   Temp");
    display.println("  Control");
  } else {
    display.println("");
    display.println("   Flow");
    display.println("  Control");
  }


}

void calculateFlow() {
  if ((millis() - lastFlowTime) > 1000)   // Only calculate flow once per second
  {
    detachInterrupt(flowInterrupt); //disable interrupt while calculating the flow
    flowRate = 0.0;

    //scale output with the milliseconds that has passed since the last calc, loop may not complete in exactly one second
    if (pulseCount != 0) {
      flowRate = (((1000.0 / (millis() - lastFlowTime)) * pulseCount)) / calibrationFactor;
    }


    // Note the time this processing pass was executed. Note that because we've
    // disabled interrupts the millis() function won't actually be incrementing right
    // at this point, but it will still return the value it was set to just before
    // interrupts went away.
    lastFlowTime = millis();

    flowMilliLitres = (flowRate / 60) * 1000;

    totalMilliLitres += flowMilliLitres;

    pulseCount = 0; // reset pulse counter

    attachInterrupt(flowInterrupt, pulseCounter, FALLING); // enable the interrupts again
  }

}
void pulseCounter()
{
  // Increment the flow pulse counter
  pulseCount++;
}
void openValve() {
  digitalWrite(valveClosePin, LOW);
  digitalWrite(valveOpenPin, HIGH); //OPEN
  valveState = VALVE_OPENING;
}
void closeValve() {
  digitalWrite(valveOpenPin, LOW);
  digitalWrite(valveClosePin, HIGH); //CLOSE
  valveState = VALVE_CLOSING;
}
void readButtons() {
  int reading1 = digitalRead(button1Pin);
  int reading2 = digitalRead(button2Pin);

  // If the switch changed, due to noise or pressing:
  if (reading1 != lastButton1State) {
    // reset the debouncing timer
    lastButton1Time = millis();
  }
  if (reading2 != lastButton2State) {
    // reset the debouncing timer
    lastButton2Time = millis();
  }

  if ((millis() - lastButton1Time) > button1Delay) {
    // whatever the reading is at, it's been there for longer than the debounce
    // delay, so take it as the actual current state:

    // if the button state has changed:
    if (reading1 != button1State) {
      button1State = reading1;

      // only toggle if the new button state is HIGH
      if (button1State == HIGH) { // Button 1 Pressed
        changeTemp = !changeTemp;
        lastDisplayState = displayState;
        displayState = CONTROL_CHANGE;

      }
    }
  }
  if ((millis() - lastButton2Time) > button2Delay) {
    // whatever the reading is at, it's been there for longer than the debounce
    // delay, so take it as the actual current state:

    // if the button state has changed:
    if (reading2 != button2State) {
      button2State = reading2;

      // only toggle if the new button state is HIGH
      if (button2State == HIGH) { //Button 2 pressed!
        if (displayState < 2 && displayState != CONTROL_CHANGE) {
          displayState++;
        } else {
          displayState = 0;
        }
      }
    }
  }


  // save the reading. Next time through the loop, it'll be the lastButtonState:
  lastButton1State = reading1;
  lastButton2State = reading2;
}
int8_t readRotaryEncoder() {
  prevNextCode <<= 2;                             // shift bits 2 positions left
  if (digitalRead(pinB)) prevNextCode |= 0x02;    // 0010
  if (digitalRead(pinA)) prevNextCode |= 0x01;     // 0001  Bitwise OR
  prevNextCode &= 0x0F;                           // 1111  Bitwise AND

  // If valid then storeCode as 16 bit pinB.
  if  (rotEncTable[prevNextCode] ) {
    storeCode <<= 4;
    storeCode |= prevNextCode;
    if ((storeCode & 0xFF) == 0x2B) return -1; // 0xFF = 11111111     0x2B = 101011
    if ((storeCode & 0xFF) == 0x17) return 1;  // 0x17 = 10111
  }
  return 0;
}

void doRotaryEncoder() {
  if ( encoderVal = readRotaryEncoder() ) {
    if ( prevNextCode == 0x0B) { //CCW Rotation  0x0B = 1011
      //Serial.print("CCW");
      if (changeTemp) {
        tempSetpoint--;
      } else {
        closeValve();
      }
    }

    if ( prevNextCode == 0x07) { // CW Rotation  0x07 = 0111
      //Serial.print("CW");
      if (changeTemp) {
        tempSetpoint++;
      } else {
        openValve();
      }

    }
  }
}

void checkValve() {
  currentValveTime = millis();
  if (currentValveTime - lastValveTime >= valveDelay)
  {
    switch (valveState)
    {
      case VALVE_IDLE:
        //do nothing
        break;
      case VALVE_OPENING:
        digitalWrite(valveOpenPin, LOW);
        break;
      case VALVE_CLOSING:
        digitalWrite(valveClosePin, LOW);
        break;
      default:
        // ?
        break;
    }
    lastValveTime = currentValveTime;
  }
}

void checkRelay() {
  currentRelayTime = millis();
  if (currentRelayTime - lastRelayTime >= relayDelay)
  {
    if (changeTemp)  //
    {
      currentTempControlTime = millis();
      if ((int)tempOUTDisp < tempSetpoint && flowRate > 3.60) {   // Just for close valve
        if ((currentTempControlTime - lastTempControlTime >= tempControlDelay)) { //
          closeValve(); // reduce flow
          lastTempControlTime = currentTempControlTime;
        }
      }
      if ((int)tempOUTDisp < tempSetpoint && !relayOn && flowRate > 0.00) {
        digitalWrite(relayPin, HIGH);
        relayOnStartTime = millis();
        relayOn = true;


      } else if ((int)tempOUTDisp > tempSetpoint || flowRate < 0.2)  {
        digitalWrite(relayPin, LOW);
        //relayOnCurrentTime = millis();
        //relayOnElapsedTime += relayOnCurrentTime - relayOnStartTime;
        relayOn = false;
        if (currentTempControlTime - lastTempControlTime >= tempControlDelay) {
          openValve(); // increase flow
          lastTempControlTime = currentTempControlTime;
        }

      }

    } else if (!relayOn && !changeTemp && flowRate > 0.2) {
      digitalWrite(relayPin, HIGH);
      relayOnStartTime = millis();
      //relayOnCurrentTime = millis();
      //relayOnElapsedTime += relayOnCurrentTime - lastRelayTime;
      relayOn = true;

    }
    if (!changeTemp && flowRate == 0.0) {
      digitalWrite(relayPin, LOW);
      relayOn = false;
    }
    lastRelayTime = currentRelayTime;

  }

  if (relayOn) {
    relayOnCurrentTime = millis();
    relayOnElapsedTime = relayOnCurrentTime - relayOnStartTime;
  }
}

void findRMSCurrent() {

  currentCurrentTime = millis();
  if (currentCurrentTime - lastCurrentTime >= currentDelay)
  {
    // calculate RMS current

    adcCurrent = analogRead(currentTransformerPin);
    //Serial.print("ADC Reading: ");
    // Serial.print(adcCurrent);
    sampleCurrent = map((float)adcCurrent, adc_curr_min, adc_curr_max, -27900, 27900); // normalise
    // Serial.print(" Sample Current: ");
    // Serial.println(sampleCurrent);

    rmsCurrentSum = rmsCurrentSum + (sampleCurrent * sampleCurrent);
    currentSampleCounter++;

    lastCurrentTime = currentCurrentTime;
  }

  if (currentSampleCounter == currentN) {

    currentRMS = sqrt(rmsCurrentSum / currentN);
    currentRMS = currentRMS / 1000.0;
    if (currentRMS <= 1.00) {
      currentRMS = 0.00;
    }

    if (relayOnElapsedTime - lastElapsedTime > 0) {
      Wh += ((230.00 * currentRMS)) * ((float)(relayOnElapsedTime - lastElapsedTime) / (float)oneHour);
    } else {
      Wh += ((230.00 * currentRMS)) * ((float)relayOnElapsedTime / (float)oneHour);
    }
    lastElapsedTime = relayOnElapsedTime;
    /* Serial.print("Apparent Power: ");
      Serial.print(currentRMS * 230.00);
      Serial.print("     RMS Current: ");
      Serial.println(currentRMS);*/
    rmsCurrentSum = 0;
    currentSampleCounter = 0;
  }

}
